package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	port := 8073 // Ganti 2 angka belakang port dengan 2 angka belakang NPM kalian masing-masing

	http.Handle("/", http.FileServer(http.Dir("polymer")))
	fmt.Printf("Starting server on port %d", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
