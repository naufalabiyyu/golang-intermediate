package main

import (
 "gitlab.com/Lepkom/pert4/naufal_14118773/handler" //sesuaikan dengan nama folder (case sensitive)
 "log"
 "net/http"
)

func main() {
 	http.HandleFunc("/api/", handler.API)

 	//Ganti 2 digit akhir port dengan 2 digit akhir NPM anda
	log.Println("localhost : 8073")
 	http.ListenAndServe(":8073", nil)
}